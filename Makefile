COMPILER_FLAGS = -Wall -g -O3
LINKER_FLAGS = -lcurl

OBJ_NAME = main

all:
	$(CXX) main.cpp $(COMPILER_FLAGS) $(LINKER_FLAGS) -o $(OBJ_NAME)
