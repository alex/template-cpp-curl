#include <iostream>
#include <string>

#include <curl/curl.h>

size_t CurlWrite_CallbackFunc_StdString(void* contents, size_t size, size_t nmemb, std::string *s)
{
    size_t newLength = size * nmemb;
    try {
        s->append((char*)contents, newLength);
    } catch (std::bad_alloc &e) {
        return 0;
    }
    return newLength;
}

int main(int argc, char* argv[])
{
    CURL *curl;
    CURLcode res;

    curl_global_init(CURL_GLOBAL_DEFAULT);
    curl = curl_easy_init();
    std::string s;

    if (curl) {
        curl_easy_setopt(curl, CURLOPT_URL, "https://manch.as/");

        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0l);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, CurlWrite_CallbackFunc_StdString);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &s);

        res = curl_easy_perform(curl);
        curl_easy_cleanup(curl);
    }

    std::cout << s << std::endl;

    return 0;
}
